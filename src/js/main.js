$(document).ready(function () {
  $(document).on("click", ".toggle_password span", function () {
    $(this).parent().toggleClass("show");
    var x = $(this).prev()[0];
    if (x.type === "password") {
      x.type = "text";
    } else {
      x.type = "password";
    }
  });

  // $(document).on('click', '.hamburger', function() {
  //     $('body').toggleClass('menu')
  // })

  // $(document).on('click', 'body', function(e) {
  //     if (!$(e.target).is('.hamburger, .hamburger *, .sidebar__header main, .sidebar__header main *')) {
  //         $('body').removeClass('menu')

  //     }
  // })

  // $(document).on('click', 'body', function(e){
  //     if (!$(e.target).is('.select__main .top *, .select__main .top')) {
  //         $('.select__main').removeClass('opened')
  //     }
  // })
  var swiper = new Swiper(".swiper_news .swiper-container", {
    slidesPerView: 3,
    spaceBetween: 16,
    autoplay: {
      delay: 12000,
    },
    loop: true,
    pagination: {
      el: ".swiper_news .swiper-pagination",
      type: "bullets",
      clickable: true,
    },
    navigation: {
      nextEl: ".swiper_news .swiper-button-next",
      prevEl: ".swiper_news .swiper-button-prev",
    },
    breakpoints: {
      767: {
        slidesPerView: 1,
      },
      991: {
        slidesPerView: 2,
      },
      1250: {
        slidesPerView: 2,
      },
    },
  });
  // language
  const menuToggle = document.querySelector("#menu-toggle");
  const imgOne = document.querySelector("#menu-one");
  const imgTwo = document.querySelector("#menu-two");
  const menuList = document.querySelector("#menu-list");
  const group = document.querySelector("#group");
  const language = document.querySelector("#langimg");
  const languageBlock = document.querySelector("#language_block");
  const searchBox = document.querySelector(".search-box");
  const searchImg = document.querySelector(".search-img");
  const search = document.querySelector(".search");
  const korzinkaInfo = document.querySelector("#korzinka_info");
  const korzinka = document.querySelector("#korzinka");
  const korzinkaImg = document.querySelector("#korzinka_img");
  const signIn = document.querySelector("#sign-in");
  const signBlock = document.querySelector("#sign-block");
  const signBlockImg = document.querySelector("#sign-in-img");
  let footerAndBody =
    80 +
    $("footer").height() +
    parseInt($("footer").css("padding-top")) +
    parseInt($("footer").css("padding-bottom"));
  $(".accaunt-all").css({
    "min-height": "calc(100vh - " + footerAndBody + "px)",
  });
  if (signIn) {
    signIn.addEventListener("click", function () {
      if (signBlock.style.display == "block") {
        signBlock.style.display = "none";
      } else {
        signBlock.style.display = "block";
      }
    });
  }

  searchImg.addEventListener("click", function () {
    if (searchBox.value != "") {
      searchImg.style.cursor = "pointer";
      searchBox.value = "";
    } else {
      searchBox.focus();
      searchImg.style.cursor = "pointer";
    }
  });

  menuToggle.addEventListener("click", function () {
    if (imgOne.style.display == "none") {
      imgOne.style.display = "block";
      imgTwo.style.display = "none";
      menuList.style.display = "none";
    } else {
      imgOne.style.display = "none";
      imgTwo.style.display = "block";
      menuList.style.display = "flex";
    }
  });
  language.addEventListener("click", function (e) {
    if (languageBlock.style.display == "block") {
      languageBlock.style.display = "none";
    } else {
      languageBlock.style.display = "block";
    }
  });
  document.body.addEventListener("click", function (e) {
    if (e.target?.id != "menu-one" && e.target?.id != "menu-two") {
      if (imgOne.style.display == "none") {
        imgOne.style.display = "block";
        imgTwo.style.display = "none";
        menuList.style.display = "none";
      }
    }
    if (e.target?.id != "langimg") {
      if (languageBlock.style.display == "block") {
        languageBlock.style.display = "none";
      }
    }
    if (e.target?.id != "korzinka_img") {
      if (korzinkaInfo.style.display == "block") {
        korzinkaInfo.style.display = "none";
      } else {
        korzinkaInfo.style.display = "none";
      }
    }
    if (e.target?.id != "sign-in-img") {
      if (signBlock) {
        if (signBlock.style.display == "block") {
          signBlock.style.display = "none";
        }
      }
    }
  });

  korzinka.addEventListener("click", function () {
    if (korzinkaInfo.style.display == "block") {
      korzinkaInfo.style.display = "none";
    } else {
      korzinkaInfo.style.display = "block";
    }
  });
  if (document.getElementById("phone")) {
    const phone = document.getElementById("phone");
    const maskOptions = {
      mask: "+{998}(00)000-00-00",
      lazy: true,
    };
    const mask = new IMask(phone, maskOptions);
  }
});
